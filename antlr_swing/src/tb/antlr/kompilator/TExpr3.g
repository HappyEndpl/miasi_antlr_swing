tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer ifNumer = 0;
}
prog    : (e+=expr | d+=decl| b+=bloc)* -> start(n={$e},d={$d});

decl  :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {locals.getSymbol($i1.text);} -> zmienna(p1={$i1.text},p2={$e2.st})
        | ^(IF e1=expr e2=expr e3=expr?) {ifNumer++;} 
          -> if(p1={$e1.st},p2={$e2.st},p3={$e3.st}, n={ifNumer.toString()})
        | ^(EQL    e1=expr e2=expr) {ifNumer++;} -> rowne(p1={$e1.st}, p2={$e2.st}, n={ifNumer.toString()})
        | ^(NEQL    e1=expr e2=expr) {ifNumer++;} -> nierowne(p1={$e1.st}, p2={$e2.st}, n={ifNumer.toString()})
        | INT                      -> int(i={$INT.text})
        | i1=ID                       {locals.getSymbol($i1.text);} -> id(n={$ID.text})

    ;
    
blok : OW    {otworz();}
       | ZW    {zamknij();};   
       
bloc    : ^(OW {locals.enterScope();} (e+=bloc | e+=expr | d+=decl)* {locals.leaveScope();}) 

          -> template(name={$e},deklaracje={$d}) "<deklaracje> <name;separator=\"\n\"> ";
       