grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}
 
prog
    : (stat | blok)+ EOF!;

blok : OW^ (stat | blok)* ZW!;

stat 
    : expr NL -> expr
    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    //print zrealizowany z nawiasami - print(23); -> 23
    | PRINT LP expr RP NL -> ^(PRINT expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    //LAB KOMPILATOR
    | if_stat NL -> if_stat
    | NL ->
    ;

// LAB KOMPILATOR
if_stat
    : IF^ expr THEN! (expr) (ELSE! (expr))?;

expr
    : logicExpr ( EQL^ logicExpr | NEQL^ logicExpr | PLUS^ logicExpr | MINUS^ logicExpr )*;

logicExpr
    : multExpr 
      ( AND^ multExpr
      | OR^ multExpr
      | NAND^ multExpr
      | NOR^ multExpr
      | BITAND^ multExpr
      | BITOR^ multExpr
      | BITNAND^ multExpr
      | BITNOR^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | POW^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

//LAB KOMPILATOR

OW : '{' ;

ZW : '}' ;

IF : 'if';

ELSE : 'else';

THEN : 'then';

EQL : '==';

NEQL : '!=';

VAR :'var';

PRINT : 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'| '\n'| ';' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP : '(' ;

RP : ')' ;

PODST :	'=' ;

PLUS :	'+' ;

MINUS :	'-' ;

MUL	:	'*' ;

DIV :	'/' ;
	
POW : '^' ;
  
AND : '&&' ;

BITAND : '&' ;

OR : '||' ;

BITOR: '|' ;

NAND : '$$' ;

BITNAND: '$' ;

NOR : '##' ;

BITNOR: '#' ;