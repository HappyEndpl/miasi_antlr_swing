tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

//prog    : (e=expr {System.out.println("Wynik operacji: " + $e.text + " to ");})* ;
prog    : (expr)*;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;} //dodawanie
	      //| ^(MINUS e1=expr) {$out = minus($e1.out);} //kontrola czy pierwsza liczba jest ujemna ~ działa tylko dla -X+X -X-X nie
	      | ^(MINUS e1=expr e2=expr) {$out = minus($e1.out, $e2.out);} //odejmowanie
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;} //mnożenie
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);} //dzielenie
        | ^(POW   e1=expr e2=expr) {$out = power($e1.out, $e2.out);} //potęgowanie
        //| ^(PODST i1=ID   e2=expr)
        | ^(AND   e1=expr e2=expr) {$out = and($e1.out, $e2.out);}
        | ^(OR    e1=expr e2=expr) {$out = or($e1.out, $e2.out);} 
        | ^(NAND  e1=expr e2=expr) {$out = nand($e1.out, $e2.out);}
        | ^(NOR  e1=expr e2=expr) {$out = nor($e1.out, $e2.out);}
        | ^(BITAND   e1=expr e2=expr) {$out = $e1.out & $e2.out;}
        | ^(BITOR    e1=expr e2=expr) {$out = $e1.out | $e2.out;} 
        | ^(BITNAND  e1=expr e2=expr) {$out = ~($e1.out & $e2.out);}
        | ^(BITNOR  e1=expr e2=expr) {$out = ~($e1.out | $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ^(VAR n=ID)           {declareVar($n.text);}
        | n=ID                {$out = getVar($n.text);}
        | ^(PODST n=ID e1=expr) {setVar($n.text, $e1.out);}
        | ^(PRINT e1=expr)         {print($e1.out.toString());}
        | OW {scope.enterScope();}
        | ZW {scope.leaveScope();}
        ;
         catch [RuntimeException ex] {System.err.println(ex);}