package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.*;

public class MyTreeParser extends TreeParser {
	
	protected LocalSymbols scope = new LocalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

//    protected void drukuj(String text) {
//        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
//    }

    protected void print (String text) {
		System.out.println(text);
	}
    
    protected Integer divide(int a, int b) {
		if(b == 0) {
			System.out.println("Operation not allowed, can't divide by 0");
			throw new RuntimeException("Operation not allowed, can't divide by 0");
		}
		return a/b;
	}
    
    protected Integer power(int a, int b) {
    	return (int)Math.pow(a, b);
    }
    
    protected Integer minus(int a, int b) {
    	return a-b;
    }
    
    protected Integer and(int a, int b) {
    	if(a != 0 && a != 1 || b != 1 && b != 0)
    	{
    		System.out.println("Logic And works only with arguments: 1 and 0");
    		throw new RuntimeException("Logic And works only with arguments: 1 and 0");
    	}
    	return a == 1 ? b == 1 ? 1 : 0 : 0;
    }
    
    protected Integer or(int a, int b) {
    	if(a != 0 && a != 1 || b != 1 && b != 0)
    	{
    		System.out.println("Logic Or works only with arguments: 1 and 0");
    		throw new RuntimeException("Logic Or works only with arguments: 1 and 0");
    	}
    	return a == 1 ? b == 1 ? 1 : 0 : b == 1 ? 1 : 0;
    }
    
    protected Integer nand(int a, int b) {
    	if(a != 0 && a != 1 || b != 1 && b != 0)
    	{
    		System.out.println("Logic Nand works only with arguments: 1 and 0");
    		throw new RuntimeException("Logic Nand works only with arguments: 1 and 0");
    	}
    	return a == 1 ? b == 1 ? 0 : 1 : 1;
    }
    
    protected Integer nor(int a, int b) {
    	if(a != 0 && a != 1 || b != 1 && b != 0)
    	{
    		System.out.println("Logic Nor works only with arguments: 1 and 0");
    		throw new RuntimeException("Logic Nor works only with arguments: 1 and 0");
    	}
    	return a == 0 ? b == 0 ? 1 : 0 : 0;
    }
    
//    protected Integer minus(int a) {
//    	return -a;
//    }
    
	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void setVar(String n, int v) {
		scope.setSymbol(n, v);
	}

	protected Integer getVar(String n) {
		return scope.getSymbol(n);
	}

	protected void declareVar(String n) {
		scope.newSymbol(n);
	}
	
}
