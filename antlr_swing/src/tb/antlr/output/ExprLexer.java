// $ANTLR 3.5.1 /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g 2021-03-17 08:27:25

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class ExprLexer extends Lexer {
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int BITAND=5;
	public static final int BITNAND=6;
	public static final int BITNOR=7;
	public static final int BITOR=8;
	public static final int DIV=9;
	public static final int ID=10;
	public static final int INT=11;
	public static final int LP=12;
	public static final int MINUS=13;
	public static final int MUL=14;
	public static final int NAND=15;
	public static final int NL=16;
	public static final int NOR=17;
	public static final int OR=18;
	public static final int OW=19;
	public static final int PLUS=20;
	public static final int PODST=21;
	public static final int POW=22;
	public static final int PRINT=23;
	public static final int RP=24;
	public static final int VAR=25;
	public static final int WS=26;
	public static final int ZW=27;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public ExprLexer() {} 
	public ExprLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public ExprLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g"; }

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:65:5: ( 'var' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:65:6: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			int _type = PRINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:67:7: ( 'print' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:67:9: 'print'
			{
			match("print"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PRINT"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:69:4: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:69:6: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:69:30: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:71:5: ( ( '0' .. '9' )+ )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:71:7: ( '0' .. '9' )+
			{
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:71:7: ( '0' .. '9' )+
			int cnt2=0;
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt2 >= 1 ) break loop2;
					EarlyExitException eee = new EarlyExitException(2, input);
					throw eee;
				}
				cnt2++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "NL"
	public final void mNL() throws RecognitionException {
		try {
			int _type = NL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:74:4: ( '\\r' | '\\n' | ';' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:
			{
			if ( input.LA(1)=='\n'||input.LA(1)=='\r'||input.LA(1)==';' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NL"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:76:4: ( ( ' ' | '\\t' )+ )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:76:6: ( ' ' | '\\t' )+
			{
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:76:6: ( ' ' | '\\t' )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( (LA3_0=='\t'||LA3_0==' ') ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			_channel = HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "LP"
	public final void mLP() throws RecognitionException {
		try {
			int _type = LP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:78:4: ( '(' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:78:6: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LP"

	// $ANTLR start "RP"
	public final void mRP() throws RecognitionException {
		try {
			int _type = RP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:80:4: ( ')' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:80:6: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RP"

	// $ANTLR start "PODST"
	public final void mPODST() throws RecognitionException {
		try {
			int _type = PODST;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:82:7: ( '=' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:82:9: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PODST"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:84:6: ( '+' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:84:8: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:86:7: ( '-' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:86:9: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MUL"
	public final void mMUL() throws RecognitionException {
		try {
			int _type = MUL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:88:5: ( '*' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:88:7: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MUL"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:90:5: ( '/' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:90:7: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "POW"
	public final void mPOW() throws RecognitionException {
		try {
			int _type = POW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:92:5: ( '^' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:92:7: '^'
			{
			match('^'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "POW"

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:94:5: ( '&&' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:94:7: '&&'
			{
			match("&&"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "BITAND"
	public final void mBITAND() throws RecognitionException {
		try {
			int _type = BITAND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:96:8: ( '&' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:96:10: '&'
			{
			match('&'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BITAND"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:98:4: ( '||' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:98:6: '||'
			{
			match("||"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "BITOR"
	public final void mBITOR() throws RecognitionException {
		try {
			int _type = BITOR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:100:6: ( '|' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:100:8: '|'
			{
			match('|'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BITOR"

	// $ANTLR start "NAND"
	public final void mNAND() throws RecognitionException {
		try {
			int _type = NAND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:102:6: ( '$$' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:102:8: '$$'
			{
			match("$$"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NAND"

	// $ANTLR start "BITNAND"
	public final void mBITNAND() throws RecognitionException {
		try {
			int _type = BITNAND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:104:8: ( '$' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:104:10: '$'
			{
			match('$'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BITNAND"

	// $ANTLR start "NOR"
	public final void mNOR() throws RecognitionException {
		try {
			int _type = NOR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:106:5: ( '##' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:106:7: '##'
			{
			match("##"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOR"

	// $ANTLR start "BITNOR"
	public final void mBITNOR() throws RecognitionException {
		try {
			int _type = BITNOR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:108:7: ( '#' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:108:9: '#'
			{
			match('#'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BITNOR"

	// $ANTLR start "OW"
	public final void mOW() throws RecognitionException {
		try {
			int _type = OW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:110:3: ( '{' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:110:5: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OW"

	// $ANTLR start "ZW"
	public final void mZW() throws RecognitionException {
		try {
			int _type = ZW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:112:3: ( '}' )
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:112:5: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ZW"

	@Override
	public void mTokens() throws RecognitionException {
		// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:8: ( VAR | PRINT | ID | INT | NL | WS | LP | RP | PODST | PLUS | MINUS | MUL | DIV | POW | AND | BITAND | OR | BITOR | NAND | BITNAND | NOR | BITNOR | OW | ZW )
		int alt4=24;
		switch ( input.LA(1) ) {
		case 'v':
			{
			int LA4_1 = input.LA(2);
			if ( (LA4_1=='a') ) {
				int LA4_21 = input.LA(3);
				if ( (LA4_21=='r') ) {
					int LA4_31 = input.LA(4);
					if ( ((LA4_31 >= '0' && LA4_31 <= '9')||(LA4_31 >= 'A' && LA4_31 <= 'Z')||LA4_31=='_'||(LA4_31 >= 'a' && LA4_31 <= 'z')) ) {
						alt4=3;
					}

					else {
						alt4=1;
					}

				}

				else {
					alt4=3;
				}

			}

			else {
				alt4=3;
			}

			}
			break;
		case 'p':
			{
			int LA4_2 = input.LA(2);
			if ( (LA4_2=='r') ) {
				int LA4_22 = input.LA(3);
				if ( (LA4_22=='i') ) {
					int LA4_32 = input.LA(4);
					if ( (LA4_32=='n') ) {
						int LA4_34 = input.LA(5);
						if ( (LA4_34=='t') ) {
							int LA4_35 = input.LA(6);
							if ( ((LA4_35 >= '0' && LA4_35 <= '9')||(LA4_35 >= 'A' && LA4_35 <= 'Z')||LA4_35=='_'||(LA4_35 >= 'a' && LA4_35 <= 'z')) ) {
								alt4=3;
							}

							else {
								alt4=2;
							}

						}

						else {
							alt4=3;
						}

					}

					else {
						alt4=3;
					}

				}

				else {
					alt4=3;
				}

			}

			else {
				alt4=3;
			}

			}
			break;
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
		case '_':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
		case 'g':
		case 'h':
		case 'i':
		case 'j':
		case 'k':
		case 'l':
		case 'm':
		case 'n':
		case 'o':
		case 'q':
		case 'r':
		case 's':
		case 't':
		case 'u':
		case 'w':
		case 'x':
		case 'y':
		case 'z':
			{
			alt4=3;
			}
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			{
			alt4=4;
			}
			break;
		case '\n':
		case '\r':
		case ';':
			{
			alt4=5;
			}
			break;
		case '\t':
		case ' ':
			{
			alt4=6;
			}
			break;
		case '(':
			{
			alt4=7;
			}
			break;
		case ')':
			{
			alt4=8;
			}
			break;
		case '=':
			{
			alt4=9;
			}
			break;
		case '+':
			{
			alt4=10;
			}
			break;
		case '-':
			{
			alt4=11;
			}
			break;
		case '*':
			{
			alt4=12;
			}
			break;
		case '/':
			{
			alt4=13;
			}
			break;
		case '^':
			{
			alt4=14;
			}
			break;
		case '&':
			{
			int LA4_15 = input.LA(2);
			if ( (LA4_15=='&') ) {
				alt4=15;
			}

			else {
				alt4=16;
			}

			}
			break;
		case '|':
			{
			int LA4_16 = input.LA(2);
			if ( (LA4_16=='|') ) {
				alt4=17;
			}

			else {
				alt4=18;
			}

			}
			break;
		case '$':
			{
			int LA4_17 = input.LA(2);
			if ( (LA4_17=='$') ) {
				alt4=19;
			}

			else {
				alt4=20;
			}

			}
			break;
		case '#':
			{
			int LA4_18 = input.LA(2);
			if ( (LA4_18=='#') ) {
				alt4=21;
			}

			else {
				alt4=22;
			}

			}
			break;
		case '{':
			{
			alt4=23;
			}
			break;
		case '}':
			{
			alt4=24;
			}
			break;
		default:
			NoViableAltException nvae =
				new NoViableAltException("", 4, 0, input);
			throw nvae;
		}
		switch (alt4) {
			case 1 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:10: VAR
				{
				mVAR(); 

				}
				break;
			case 2 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:14: PRINT
				{
				mPRINT(); 

				}
				break;
			case 3 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:20: ID
				{
				mID(); 

				}
				break;
			case 4 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:23: INT
				{
				mINT(); 

				}
				break;
			case 5 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:27: NL
				{
				mNL(); 

				}
				break;
			case 6 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:30: WS
				{
				mWS(); 

				}
				break;
			case 7 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:33: LP
				{
				mLP(); 

				}
				break;
			case 8 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:36: RP
				{
				mRP(); 

				}
				break;
			case 9 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:39: PODST
				{
				mPODST(); 

				}
				break;
			case 10 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:45: PLUS
				{
				mPLUS(); 

				}
				break;
			case 11 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:50: MINUS
				{
				mMINUS(); 

				}
				break;
			case 12 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:56: MUL
				{
				mMUL(); 

				}
				break;
			case 13 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:60: DIV
				{
				mDIV(); 

				}
				break;
			case 14 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:64: POW
				{
				mPOW(); 

				}
				break;
			case 15 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:68: AND
				{
				mAND(); 

				}
				break;
			case 16 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:72: BITAND
				{
				mBITAND(); 

				}
				break;
			case 17 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:79: OR
				{
				mOR(); 

				}
				break;
			case 18 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:82: BITOR
				{
				mBITOR(); 

				}
				break;
			case 19 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:88: NAND
				{
				mNAND(); 

				}
				break;
			case 20 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:93: BITNAND
				{
				mBITNAND(); 

				}
				break;
			case 21 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:101: NOR
				{
				mNOR(); 

				}
				break;
			case 22 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:105: BITNOR
				{
				mBITNOR(); 

				}
				break;
			case 23 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:112: OW
				{
				mOW(); 

				}
				break;
			case 24 :
				// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:1:115: ZW
				{
				mZW(); 

				}
				break;

		}
	}



}
