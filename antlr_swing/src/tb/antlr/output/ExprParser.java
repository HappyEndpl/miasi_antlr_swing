// $ANTLR 3.5.1 /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g 2021-03-17 08:27:24

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.debug.*;
import java.io.IOException;
import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class ExprParser extends DebugParser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "BITAND", "BITNAND", "BITNOR", 
		"BITOR", "DIV", "ID", "INT", "LP", "MINUS", "MUL", "NAND", "NL", "NOR", 
		"OR", "OW", "PLUS", "PODST", "POW", "PRINT", "RP", "VAR", "WS", "ZW"
	};
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int BITAND=5;
	public static final int BITNAND=6;
	public static final int BITNOR=7;
	public static final int BITOR=8;
	public static final int DIV=9;
	public static final int ID=10;
	public static final int INT=11;
	public static final int LP=12;
	public static final int MINUS=13;
	public static final int MUL=14;
	public static final int NAND=15;
	public static final int NL=16;
	public static final int NOR=17;
	public static final int OR=18;
	public static final int OW=19;
	public static final int PLUS=20;
	public static final int PODST=21;
	public static final int POW=22;
	public static final int PRINT=23;
	public static final int RP=24;
	public static final int VAR=25;
	public static final int WS=26;
	public static final int ZW=27;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public static final String[] ruleNames = new String[] {
		"invalidRule", "logicExpr", "multExpr", "prog", "blok", "expr", "atom", 
		"stat"
	};

	public static final boolean[] decisionCanBacktrack = new boolean[] {
		false, // invalid decision
		false, false, false, false, false, false, false
	};

 
	public int ruleLevel = 0;
	public int getRuleLevel() { return ruleLevel; }
	public void incRuleLevel() { ruleLevel++; }
	public void decRuleLevel() { ruleLevel--; }
	public ExprParser(TokenStream input) {
		this(input, DebugEventSocketProxy.DEFAULT_DEBUGGER_PORT, new RecognizerSharedState());
	}
	public ExprParser(TokenStream input, int port, RecognizerSharedState state) {
		super(input, state);
		DebugEventSocketProxy proxy =
			new DebugEventSocketProxy(this,port,adaptor);
		setDebugListener(proxy);
		setTokenStream(new DebugTokenStream(input,proxy));
		try {
			proxy.handshake();
		}
		catch (IOException ioe) {
			reportError(ioe);
		}
		TreeAdaptor adap = new CommonTreeAdaptor();
		setTreeAdaptor(adap);
		proxy.setTreeAdaptor(adap);
	}

	public ExprParser(TokenStream input, DebugEventListener dbg) {
		super(input, dbg);
		 
		TreeAdaptor adap = new CommonTreeAdaptor();
		setTreeAdaptor(adap);

	}

	protected boolean evalPredicate(boolean result, String predicate) {
		dbg.semanticPredicate(result, predicate);
		return result;
	}

		protected DebugTreeAdaptor adaptor;
		public void setTreeAdaptor(TreeAdaptor adaptor) {
			this.adaptor = new DebugTreeAdaptor(dbg,adaptor);
		}
		public TreeAdaptor getTreeAdaptor() {
			return adaptor;
		}
	@Override public String[] getTokenNames() { return ExprParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g"; }


	public static class prog_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "prog"
	// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:16:1: prog : ( stat | blok )+ EOF !;
	public final ExprParser.prog_return prog() throws RecognitionException {
		ExprParser.prog_return retval = new ExprParser.prog_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token EOF3=null;
		ParserRuleReturnScope stat1 =null;
		ParserRuleReturnScope blok2 =null;

		CommonTree EOF3_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "prog");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(16, 0);

		try {
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:5: ( ( stat | blok )+ EOF !)
			dbg.enterAlt(1);

			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat | blok )+ EOF !
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(17,7);
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat | blok )+
			int cnt1=0;
			try { dbg.enterSubRule(1);

			loop1:
			while (true) {
				int alt1=3;
				try { dbg.enterDecision(1, decisionCanBacktrack[1]);

				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= ID && LA1_0 <= LP)||LA1_0==NL||LA1_0==PRINT||LA1_0==VAR) ) {
					alt1=1;
				}
				else if ( (LA1_0==OW) ) {
					alt1=2;
				}

				} finally {dbg.exitDecision(1);}

				switch (alt1) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:8: stat
					{
					dbg.location(17,8);
					pushFollow(FOLLOW_stat_in_prog50);
					stat1=stat();
					state._fsp--;

					adaptor.addChild(root_0, stat1.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:15: blok
					{
					dbg.location(17,15);
					pushFollow(FOLLOW_blok_in_prog54);
					blok2=blok();
					state._fsp--;

					adaptor.addChild(root_0, blok2.getTree());

					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					EarlyExitException eee = new EarlyExitException(1, input);
					dbg.recognitionException(eee);

					throw eee;
				}
				cnt1++;
			}
			} finally {dbg.exitSubRule(1);}
			dbg.location(17,25);
			EOF3=(Token)match(input,EOF,FOLLOW_EOF_in_prog58); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(17, 25);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "prog");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "prog"


	public static class blok_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "blok"
	// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:1: blok : OW ^ ( stat | blok )* ZW !;
	public final ExprParser.blok_return blok() throws RecognitionException {
		ExprParser.blok_return retval = new ExprParser.blok_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token OW4=null;
		Token ZW7=null;
		ParserRuleReturnScope stat5 =null;
		ParserRuleReturnScope blok6 =null;

		CommonTree OW4_tree=null;
		CommonTree ZW7_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "blok");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(19, 0);

		try {
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:6: ( OW ^ ( stat | blok )* ZW !)
			dbg.enterAlt(1);

			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:8: OW ^ ( stat | blok )* ZW !
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(19,10);
			OW4=(Token)match(input,OW,FOLLOW_OW_in_blok67); 
			OW4_tree = (CommonTree)adaptor.create(OW4);
			root_0 = (CommonTree)adaptor.becomeRoot(OW4_tree, root_0);
			dbg.location(19,12);
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:12: ( stat | blok )*
			try { dbg.enterSubRule(2);

			loop2:
			while (true) {
				int alt2=3;
				try { dbg.enterDecision(2, decisionCanBacktrack[2]);

				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= ID && LA2_0 <= LP)||LA2_0==NL||LA2_0==PRINT||LA2_0==VAR) ) {
					alt2=1;
				}
				else if ( (LA2_0==OW) ) {
					alt2=2;
				}

				} finally {dbg.exitDecision(2);}

				switch (alt2) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:13: stat
					{
					dbg.location(19,13);
					pushFollow(FOLLOW_stat_in_blok71);
					stat5=stat();
					state._fsp--;

					adaptor.addChild(root_0, stat5.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:20: blok
					{
					dbg.location(19,20);
					pushFollow(FOLLOW_blok_in_blok75);
					blok6=blok();
					state._fsp--;

					adaptor.addChild(root_0, blok6.getTree());

					}
					break;

				default :
					break loop2;
				}
			}
			} finally {dbg.exitSubRule(2);}
			dbg.location(19,29);
			ZW7=(Token)match(input,ZW,FOLLOW_ZW_in_blok79); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(19, 29);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "blok");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "blok"


	public static class stat_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "stat"
	// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:21:1: stat : ( expr NL -> expr | VAR ID PODST expr NL -> ^( VAR ID ) ^( PODST ID expr ) | PRINT LP expr RP NL -> ^( PRINT expr ) | VAR ID NL -> ^( VAR ID ) | ID PODST expr NL -> ^( PODST ID expr ) | NL ->);
	public final ExprParser.stat_return stat() throws RecognitionException {
		ExprParser.stat_return retval = new ExprParser.stat_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token NL9=null;
		Token VAR10=null;
		Token ID11=null;
		Token PODST12=null;
		Token NL14=null;
		Token PRINT15=null;
		Token LP16=null;
		Token RP18=null;
		Token NL19=null;
		Token VAR20=null;
		Token ID21=null;
		Token NL22=null;
		Token ID23=null;
		Token PODST24=null;
		Token NL26=null;
		Token NL27=null;
		ParserRuleReturnScope expr8 =null;
		ParserRuleReturnScope expr13 =null;
		ParserRuleReturnScope expr17 =null;
		ParserRuleReturnScope expr25 =null;

		CommonTree NL9_tree=null;
		CommonTree VAR10_tree=null;
		CommonTree ID11_tree=null;
		CommonTree PODST12_tree=null;
		CommonTree NL14_tree=null;
		CommonTree PRINT15_tree=null;
		CommonTree LP16_tree=null;
		CommonTree RP18_tree=null;
		CommonTree NL19_tree=null;
		CommonTree VAR20_tree=null;
		CommonTree ID21_tree=null;
		CommonTree NL22_tree=null;
		CommonTree ID23_tree=null;
		CommonTree PODST24_tree=null;
		CommonTree NL26_tree=null;
		CommonTree NL27_tree=null;
		RewriteRuleTokenStream stream_PRINT=new RewriteRuleTokenStream(adaptor,"token PRINT");
		RewriteRuleTokenStream stream_LP=new RewriteRuleTokenStream(adaptor,"token LP");
		RewriteRuleTokenStream stream_VAR=new RewriteRuleTokenStream(adaptor,"token VAR");
		RewriteRuleTokenStream stream_PODST=new RewriteRuleTokenStream(adaptor,"token PODST");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_NL=new RewriteRuleTokenStream(adaptor,"token NL");
		RewriteRuleTokenStream stream_RP=new RewriteRuleTokenStream(adaptor,"token RP");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try { dbg.enterRule(getGrammarFileName(), "stat");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(21, 0);

		try {
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:22:5: ( expr NL -> expr | VAR ID PODST expr NL -> ^( VAR ID ) ^( PODST ID expr ) | PRINT LP expr RP NL -> ^( PRINT expr ) | VAR ID NL -> ^( VAR ID ) | ID PODST expr NL -> ^( PODST ID expr ) | NL ->)
			int alt3=6;
			try { dbg.enterDecision(3, decisionCanBacktrack[3]);

			switch ( input.LA(1) ) {
			case INT:
			case LP:
				{
				alt3=1;
				}
				break;
			case ID:
				{
				int LA3_2 = input.LA(2);
				if ( (LA3_2==PODST) ) {
					alt3=5;
				}
				else if ( ((LA3_2 >= AND && LA3_2 <= DIV)||(LA3_2 >= MINUS && LA3_2 <= OR)||LA3_2==PLUS||LA3_2==POW) ) {
					alt3=1;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 3, 2, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case VAR:
				{
				int LA3_3 = input.LA(2);
				if ( (LA3_3==ID) ) {
					int LA3_7 = input.LA(3);
					if ( (LA3_7==PODST) ) {
						alt3=2;
					}
					else if ( (LA3_7==NL) ) {
						alt3=4;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 3, 7, input);
							dbg.recognitionException(nvae);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 3, 3, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case PRINT:
				{
				alt3=3;
				}
				break;
			case NL:
				{
				alt3=6;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(3);}

			switch (alt3) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:22:7: expr NL
					{
					dbg.location(22,7);
					pushFollow(FOLLOW_expr_in_stat93);
					expr8=expr();
					state._fsp--;

					stream_expr.add(expr8.getTree());dbg.location(22,12);
					NL9=(Token)match(input,NL,FOLLOW_NL_in_stat95);  
					stream_NL.add(NL9);

					// AST REWRITE
					// elements: expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 22:15: -> expr
					{
						dbg.location(22,18);
						adaptor.addChild(root_0, stream_expr.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:7: VAR ID PODST expr NL
					{
					dbg.location(23,7);
					VAR10=(Token)match(input,VAR,FOLLOW_VAR_in_stat107);  
					stream_VAR.add(VAR10);
					dbg.location(23,11);
					ID11=(Token)match(input,ID,FOLLOW_ID_in_stat109);  
					stream_ID.add(ID11);
					dbg.location(23,14);
					PODST12=(Token)match(input,PODST,FOLLOW_PODST_in_stat111);  
					stream_PODST.add(PODST12);
					dbg.location(23,20);
					pushFollow(FOLLOW_expr_in_stat113);
					expr13=expr();
					state._fsp--;

					stream_expr.add(expr13.getTree());dbg.location(23,25);
					NL14=(Token)match(input,NL,FOLLOW_NL_in_stat115);  
					stream_NL.add(NL14);

					// AST REWRITE
					// elements: VAR, ID, expr, PODST, ID
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 23:28: -> ^( VAR ID ) ^( PODST ID expr )
					{
						dbg.location(23,31);
						// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:31: ^( VAR ID )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(23,33);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_VAR.nextNode(), root_1);
						dbg.location(23,37);
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_0, root_1);
						}
						dbg.location(23,41);
						// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:41: ^( PODST ID expr )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(23,43);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_PODST.nextNode(), root_1);
						dbg.location(23,49);
						adaptor.addChild(root_1, stream_ID.nextNode());dbg.location(23,52);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:7: PRINT LP expr RP NL
					{
					dbg.location(25,7);
					PRINT15=(Token)match(input,PRINT,FOLLOW_PRINT_in_stat144);  
					stream_PRINT.add(PRINT15);
					dbg.location(25,13);
					LP16=(Token)match(input,LP,FOLLOW_LP_in_stat146);  
					stream_LP.add(LP16);
					dbg.location(25,16);
					pushFollow(FOLLOW_expr_in_stat148);
					expr17=expr();
					state._fsp--;

					stream_expr.add(expr17.getTree());dbg.location(25,21);
					RP18=(Token)match(input,RP,FOLLOW_RP_in_stat150);  
					stream_RP.add(RP18);
					dbg.location(25,24);
					NL19=(Token)match(input,NL,FOLLOW_NL_in_stat152);  
					stream_NL.add(NL19);

					// AST REWRITE
					// elements: PRINT, expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 25:27: -> ^( PRINT expr )
					{
						dbg.location(25,30);
						// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:30: ^( PRINT expr )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(25,32);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_PRINT.nextNode(), root_1);
						dbg.location(25,38);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:26:7: VAR ID NL
					{
					dbg.location(26,7);
					VAR20=(Token)match(input,VAR,FOLLOW_VAR_in_stat168);  
					stream_VAR.add(VAR20);
					dbg.location(26,11);
					ID21=(Token)match(input,ID,FOLLOW_ID_in_stat170);  
					stream_ID.add(ID21);
					dbg.location(26,14);
					NL22=(Token)match(input,NL,FOLLOW_NL_in_stat172);  
					stream_NL.add(NL22);

					// AST REWRITE
					// elements: ID, VAR
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 26:17: -> ^( VAR ID )
					{
						dbg.location(26,20);
						// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:26:20: ^( VAR ID )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(26,22);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_VAR.nextNode(), root_1);
						dbg.location(26,26);
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 5 :
					dbg.enterAlt(5);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:27:7: ID PODST expr NL
					{
					dbg.location(27,7);
					ID23=(Token)match(input,ID,FOLLOW_ID_in_stat188);  
					stream_ID.add(ID23);
					dbg.location(27,10);
					PODST24=(Token)match(input,PODST,FOLLOW_PODST_in_stat190);  
					stream_PODST.add(PODST24);
					dbg.location(27,16);
					pushFollow(FOLLOW_expr_in_stat192);
					expr25=expr();
					state._fsp--;

					stream_expr.add(expr25.getTree());dbg.location(27,21);
					NL26=(Token)match(input,NL,FOLLOW_NL_in_stat194);  
					stream_NL.add(NL26);

					// AST REWRITE
					// elements: expr, ID, PODST
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 27:24: -> ^( PODST ID expr )
					{
						dbg.location(27,27);
						// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:27:27: ^( PODST ID expr )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(27,29);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_PODST.nextNode(), root_1);
						dbg.location(27,35);
						adaptor.addChild(root_1, stream_ID.nextNode());dbg.location(27,38);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 6 :
					dbg.enterAlt(6);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:28:7: NL
					{
					dbg.location(28,7);
					NL27=(Token)match(input,NL,FOLLOW_NL_in_stat212);  
					stream_NL.add(NL27);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 28:10: ->
					{
						dbg.location(29,5);
						root_0 = null;
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(29, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "stat");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "stat"


	public static class expr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr"
	// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:31:1: expr : logicExpr ( PLUS ^ logicExpr | MINUS ^ logicExpr )* ;
	public final ExprParser.expr_return expr() throws RecognitionException {
		ExprParser.expr_return retval = new ExprParser.expr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token PLUS29=null;
		Token MINUS31=null;
		ParserRuleReturnScope logicExpr28 =null;
		ParserRuleReturnScope logicExpr30 =null;
		ParserRuleReturnScope logicExpr32 =null;

		CommonTree PLUS29_tree=null;
		CommonTree MINUS31_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "expr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(31, 0);

		try {
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:32:5: ( logicExpr ( PLUS ^ logicExpr | MINUS ^ logicExpr )* )
			dbg.enterAlt(1);

			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:32:7: logicExpr ( PLUS ^ logicExpr | MINUS ^ logicExpr )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(32,7);
			pushFollow(FOLLOW_logicExpr_in_expr231);
			logicExpr28=logicExpr();
			state._fsp--;

			adaptor.addChild(root_0, logicExpr28.getTree());
			dbg.location(32,17);
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:32:17: ( PLUS ^ logicExpr | MINUS ^ logicExpr )*
			try { dbg.enterSubRule(4);

			loop4:
			while (true) {
				int alt4=3;
				try { dbg.enterDecision(4, decisionCanBacktrack[4]);

				int LA4_0 = input.LA(1);
				if ( (LA4_0==PLUS) ) {
					alt4=1;
				}
				else if ( (LA4_0==MINUS) ) {
					alt4=2;
				}

				} finally {dbg.exitDecision(4);}

				switch (alt4) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:32:19: PLUS ^ logicExpr
					{
					dbg.location(32,23);
					PLUS29=(Token)match(input,PLUS,FOLLOW_PLUS_in_expr235); 
					PLUS29_tree = (CommonTree)adaptor.create(PLUS29);
					root_0 = (CommonTree)adaptor.becomeRoot(PLUS29_tree, root_0);
					dbg.location(32,25);
					pushFollow(FOLLOW_logicExpr_in_expr238);
					logicExpr30=logicExpr();
					state._fsp--;

					adaptor.addChild(root_0, logicExpr30.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:32:37: MINUS ^ logicExpr
					{
					dbg.location(32,42);
					MINUS31=(Token)match(input,MINUS,FOLLOW_MINUS_in_expr242); 
					MINUS31_tree = (CommonTree)adaptor.create(MINUS31);
					root_0 = (CommonTree)adaptor.becomeRoot(MINUS31_tree, root_0);
					dbg.location(32,44);
					pushFollow(FOLLOW_logicExpr_in_expr245);
					logicExpr32=logicExpr();
					state._fsp--;

					adaptor.addChild(root_0, logicExpr32.getTree());

					}
					break;

				default :
					break loop4;
				}
			}
			} finally {dbg.exitSubRule(4);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(35, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "expr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "expr"


	public static class logicExpr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "logicExpr"
	// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:37:1: logicExpr : multExpr ( AND ^ multExpr | OR ^ multExpr | NAND ^ multExpr | NOR ^ multExpr | BITAND ^ multExpr | BITOR ^ multExpr | BITNAND ^ multExpr | BITNOR ^ multExpr )* ;
	public final ExprParser.logicExpr_return logicExpr() throws RecognitionException {
		ExprParser.logicExpr_return retval = new ExprParser.logicExpr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token AND34=null;
		Token OR36=null;
		Token NAND38=null;
		Token NOR40=null;
		Token BITAND42=null;
		Token BITOR44=null;
		Token BITNAND46=null;
		Token BITNOR48=null;
		ParserRuleReturnScope multExpr33 =null;
		ParserRuleReturnScope multExpr35 =null;
		ParserRuleReturnScope multExpr37 =null;
		ParserRuleReturnScope multExpr39 =null;
		ParserRuleReturnScope multExpr41 =null;
		ParserRuleReturnScope multExpr43 =null;
		ParserRuleReturnScope multExpr45 =null;
		ParserRuleReturnScope multExpr47 =null;
		ParserRuleReturnScope multExpr49 =null;

		CommonTree AND34_tree=null;
		CommonTree OR36_tree=null;
		CommonTree NAND38_tree=null;
		CommonTree NOR40_tree=null;
		CommonTree BITAND42_tree=null;
		CommonTree BITOR44_tree=null;
		CommonTree BITNAND46_tree=null;
		CommonTree BITNOR48_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "logicExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(37, 0);

		try {
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:38:5: ( multExpr ( AND ^ multExpr | OR ^ multExpr | NAND ^ multExpr | NOR ^ multExpr | BITAND ^ multExpr | BITOR ^ multExpr | BITNAND ^ multExpr | BITNOR ^ multExpr )* )
			dbg.enterAlt(1);

			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:38:7: multExpr ( AND ^ multExpr | OR ^ multExpr | NAND ^ multExpr | NOR ^ multExpr | BITAND ^ multExpr | BITOR ^ multExpr | BITNAND ^ multExpr | BITNOR ^ multExpr )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(38,7);
			pushFollow(FOLLOW_multExpr_in_logicExpr279);
			multExpr33=multExpr();
			state._fsp--;

			adaptor.addChild(root_0, multExpr33.getTree());
			dbg.location(39,7);
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:39:7: ( AND ^ multExpr | OR ^ multExpr | NAND ^ multExpr | NOR ^ multExpr | BITAND ^ multExpr | BITOR ^ multExpr | BITNAND ^ multExpr | BITNOR ^ multExpr )*
			try { dbg.enterSubRule(5);

			loop5:
			while (true) {
				int alt5=9;
				try { dbg.enterDecision(5, decisionCanBacktrack[5]);

				switch ( input.LA(1) ) {
				case AND:
					{
					alt5=1;
					}
					break;
				case OR:
					{
					alt5=2;
					}
					break;
				case NAND:
					{
					alt5=3;
					}
					break;
				case NOR:
					{
					alt5=4;
					}
					break;
				case BITAND:
					{
					alt5=5;
					}
					break;
				case BITOR:
					{
					alt5=6;
					}
					break;
				case BITNAND:
					{
					alt5=7;
					}
					break;
				case BITNOR:
					{
					alt5=8;
					}
					break;
				}
				} finally {dbg.exitDecision(5);}

				switch (alt5) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:39:9: AND ^ multExpr
					{
					dbg.location(39,12);
					AND34=(Token)match(input,AND,FOLLOW_AND_in_logicExpr290); 
					AND34_tree = (CommonTree)adaptor.create(AND34);
					root_0 = (CommonTree)adaptor.becomeRoot(AND34_tree, root_0);
					dbg.location(39,14);
					pushFollow(FOLLOW_multExpr_in_logicExpr293);
					multExpr35=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr35.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:40:9: OR ^ multExpr
					{
					dbg.location(40,11);
					OR36=(Token)match(input,OR,FOLLOW_OR_in_logicExpr303); 
					OR36_tree = (CommonTree)adaptor.create(OR36);
					root_0 = (CommonTree)adaptor.becomeRoot(OR36_tree, root_0);
					dbg.location(40,13);
					pushFollow(FOLLOW_multExpr_in_logicExpr306);
					multExpr37=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr37.getTree());

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:41:9: NAND ^ multExpr
					{
					dbg.location(41,13);
					NAND38=(Token)match(input,NAND,FOLLOW_NAND_in_logicExpr316); 
					NAND38_tree = (CommonTree)adaptor.create(NAND38);
					root_0 = (CommonTree)adaptor.becomeRoot(NAND38_tree, root_0);
					dbg.location(41,15);
					pushFollow(FOLLOW_multExpr_in_logicExpr319);
					multExpr39=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr39.getTree());

					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:42:9: NOR ^ multExpr
					{
					dbg.location(42,12);
					NOR40=(Token)match(input,NOR,FOLLOW_NOR_in_logicExpr329); 
					NOR40_tree = (CommonTree)adaptor.create(NOR40);
					root_0 = (CommonTree)adaptor.becomeRoot(NOR40_tree, root_0);
					dbg.location(42,14);
					pushFollow(FOLLOW_multExpr_in_logicExpr332);
					multExpr41=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr41.getTree());

					}
					break;
				case 5 :
					dbg.enterAlt(5);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:43:9: BITAND ^ multExpr
					{
					dbg.location(43,15);
					BITAND42=(Token)match(input,BITAND,FOLLOW_BITAND_in_logicExpr342); 
					BITAND42_tree = (CommonTree)adaptor.create(BITAND42);
					root_0 = (CommonTree)adaptor.becomeRoot(BITAND42_tree, root_0);
					dbg.location(43,17);
					pushFollow(FOLLOW_multExpr_in_logicExpr345);
					multExpr43=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr43.getTree());

					}
					break;
				case 6 :
					dbg.enterAlt(6);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:44:9: BITOR ^ multExpr
					{
					dbg.location(44,14);
					BITOR44=(Token)match(input,BITOR,FOLLOW_BITOR_in_logicExpr355); 
					BITOR44_tree = (CommonTree)adaptor.create(BITOR44);
					root_0 = (CommonTree)adaptor.becomeRoot(BITOR44_tree, root_0);
					dbg.location(44,16);
					pushFollow(FOLLOW_multExpr_in_logicExpr358);
					multExpr45=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr45.getTree());

					}
					break;
				case 7 :
					dbg.enterAlt(7);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:45:9: BITNAND ^ multExpr
					{
					dbg.location(45,16);
					BITNAND46=(Token)match(input,BITNAND,FOLLOW_BITNAND_in_logicExpr368); 
					BITNAND46_tree = (CommonTree)adaptor.create(BITNAND46);
					root_0 = (CommonTree)adaptor.becomeRoot(BITNAND46_tree, root_0);
					dbg.location(45,18);
					pushFollow(FOLLOW_multExpr_in_logicExpr371);
					multExpr47=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr47.getTree());

					}
					break;
				case 8 :
					dbg.enterAlt(8);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:46:9: BITNOR ^ multExpr
					{
					dbg.location(46,15);
					BITNOR48=(Token)match(input,BITNOR,FOLLOW_BITNOR_in_logicExpr381); 
					BITNOR48_tree = (CommonTree)adaptor.create(BITNOR48);
					root_0 = (CommonTree)adaptor.becomeRoot(BITNOR48_tree, root_0);
					dbg.location(46,17);
					pushFollow(FOLLOW_multExpr_in_logicExpr384);
					multExpr49=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr49.getTree());

					}
					break;

				default :
					break loop5;
				}
			}
			} finally {dbg.exitSubRule(5);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(48, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "logicExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "logicExpr"


	public static class multExpr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "multExpr"
	// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:50:1: multExpr : atom ( MUL ^ atom | DIV ^ atom | POW ^ atom )* ;
	public final ExprParser.multExpr_return multExpr() throws RecognitionException {
		ExprParser.multExpr_return retval = new ExprParser.multExpr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token MUL51=null;
		Token DIV53=null;
		Token POW55=null;
		ParserRuleReturnScope atom50 =null;
		ParserRuleReturnScope atom52 =null;
		ParserRuleReturnScope atom54 =null;
		ParserRuleReturnScope atom56 =null;

		CommonTree MUL51_tree=null;
		CommonTree DIV53_tree=null;
		CommonTree POW55_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "multExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(50, 0);

		try {
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:51:5: ( atom ( MUL ^ atom | DIV ^ atom | POW ^ atom )* )
			dbg.enterAlt(1);

			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:51:7: atom ( MUL ^ atom | DIV ^ atom | POW ^ atom )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(51,7);
			pushFollow(FOLLOW_atom_in_multExpr410);
			atom50=atom();
			state._fsp--;

			adaptor.addChild(root_0, atom50.getTree());
			dbg.location(52,7);
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:52:7: ( MUL ^ atom | DIV ^ atom | POW ^ atom )*
			try { dbg.enterSubRule(6);

			loop6:
			while (true) {
				int alt6=4;
				try { dbg.enterDecision(6, decisionCanBacktrack[6]);

				switch ( input.LA(1) ) {
				case MUL:
					{
					alt6=1;
					}
					break;
				case DIV:
					{
					alt6=2;
					}
					break;
				case POW:
					{
					alt6=3;
					}
					break;
				}
				} finally {dbg.exitDecision(6);}

				switch (alt6) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:52:9: MUL ^ atom
					{
					dbg.location(52,12);
					MUL51=(Token)match(input,MUL,FOLLOW_MUL_in_multExpr420); 
					MUL51_tree = (CommonTree)adaptor.create(MUL51);
					root_0 = (CommonTree)adaptor.becomeRoot(MUL51_tree, root_0);
					dbg.location(52,14);
					pushFollow(FOLLOW_atom_in_multExpr423);
					atom52=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom52.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:53:9: DIV ^ atom
					{
					dbg.location(53,12);
					DIV53=(Token)match(input,DIV,FOLLOW_DIV_in_multExpr433); 
					DIV53_tree = (CommonTree)adaptor.create(DIV53);
					root_0 = (CommonTree)adaptor.becomeRoot(DIV53_tree, root_0);
					dbg.location(53,14);
					pushFollow(FOLLOW_atom_in_multExpr436);
					atom54=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom54.getTree());

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:54:9: POW ^ atom
					{
					dbg.location(54,12);
					POW55=(Token)match(input,POW,FOLLOW_POW_in_multExpr446); 
					POW55_tree = (CommonTree)adaptor.create(POW55);
					root_0 = (CommonTree)adaptor.becomeRoot(POW55_tree, root_0);
					dbg.location(54,14);
					pushFollow(FOLLOW_atom_in_multExpr449);
					atom56=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom56.getTree());

					}
					break;

				default :
					break loop6;
				}
			}
			} finally {dbg.exitSubRule(6);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(56, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "multExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "multExpr"


	public static class atom_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "atom"
	// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:58:1: atom : ( INT | ID | LP ! expr RP !);
	public final ExprParser.atom_return atom() throws RecognitionException {
		ExprParser.atom_return retval = new ExprParser.atom_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token INT57=null;
		Token ID58=null;
		Token LP59=null;
		Token RP61=null;
		ParserRuleReturnScope expr60 =null;

		CommonTree INT57_tree=null;
		CommonTree ID58_tree=null;
		CommonTree LP59_tree=null;
		CommonTree RP61_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "atom");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(58, 0);

		try {
			// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:59:5: ( INT | ID | LP ! expr RP !)
			int alt7=3;
			try { dbg.enterDecision(7, decisionCanBacktrack[7]);

			switch ( input.LA(1) ) {
			case INT:
				{
				alt7=1;
				}
				break;
			case ID:
				{
				alt7=2;
				}
				break;
			case LP:
				{
				alt7=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(7);}

			switch (alt7) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:59:7: INT
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(59,7);
					INT57=(Token)match(input,INT,FOLLOW_INT_in_atom475); 
					INT57_tree = (CommonTree)adaptor.create(INT57);
					adaptor.addChild(root_0, INT57_tree);

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:60:7: ID
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(60,7);
					ID58=(Token)match(input,ID,FOLLOW_ID_in_atom483); 
					ID58_tree = (CommonTree)adaptor.create(ID58);
					adaptor.addChild(root_0, ID58_tree);

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// /home/student/git/miasi_antlr_swing/antlr_swing/src/tb/antlr/Expr.g:61:7: LP ! expr RP !
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(61,9);
					LP59=(Token)match(input,LP,FOLLOW_LP_in_atom491); dbg.location(61,11);
					pushFollow(FOLLOW_expr_in_atom494);
					expr60=expr();
					state._fsp--;

					adaptor.addChild(root_0, expr60.getTree());
					dbg.location(61,18);
					RP61=(Token)match(input,RP,FOLLOW_RP_in_atom496); 
					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(62, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "atom");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "atom"

	// Delegated rules



	public static final BitSet FOLLOW_stat_in_prog50 = new BitSet(new long[]{0x0000000002891C00L});
	public static final BitSet FOLLOW_blok_in_prog54 = new BitSet(new long[]{0x0000000002891C00L});
	public static final BitSet FOLLOW_EOF_in_prog58 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_OW_in_blok67 = new BitSet(new long[]{0x000000000A891C00L});
	public static final BitSet FOLLOW_stat_in_blok71 = new BitSet(new long[]{0x000000000A891C00L});
	public static final BitSet FOLLOW_blok_in_blok75 = new BitSet(new long[]{0x000000000A891C00L});
	public static final BitSet FOLLOW_ZW_in_blok79 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr_in_stat93 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_NL_in_stat95 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_stat107 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_ID_in_stat109 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_PODST_in_stat111 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_expr_in_stat113 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_NL_in_stat115 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_stat144 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_LP_in_stat146 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_expr_in_stat148 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_RP_in_stat150 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_NL_in_stat152 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_stat168 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_ID_in_stat170 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_NL_in_stat172 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_stat188 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_PODST_in_stat190 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_expr_in_stat192 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_NL_in_stat194 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NL_in_stat212 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_logicExpr_in_expr231 = new BitSet(new long[]{0x0000000000102002L});
	public static final BitSet FOLLOW_PLUS_in_expr235 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_logicExpr_in_expr238 = new BitSet(new long[]{0x0000000000102002L});
	public static final BitSet FOLLOW_MINUS_in_expr242 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_logicExpr_in_expr245 = new BitSet(new long[]{0x0000000000102002L});
	public static final BitSet FOLLOW_multExpr_in_logicExpr279 = new BitSet(new long[]{0x00000000000681F2L});
	public static final BitSet FOLLOW_AND_in_logicExpr290 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_multExpr_in_logicExpr293 = new BitSet(new long[]{0x00000000000681F2L});
	public static final BitSet FOLLOW_OR_in_logicExpr303 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_multExpr_in_logicExpr306 = new BitSet(new long[]{0x00000000000681F2L});
	public static final BitSet FOLLOW_NAND_in_logicExpr316 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_multExpr_in_logicExpr319 = new BitSet(new long[]{0x00000000000681F2L});
	public static final BitSet FOLLOW_NOR_in_logicExpr329 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_multExpr_in_logicExpr332 = new BitSet(new long[]{0x00000000000681F2L});
	public static final BitSet FOLLOW_BITAND_in_logicExpr342 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_multExpr_in_logicExpr345 = new BitSet(new long[]{0x00000000000681F2L});
	public static final BitSet FOLLOW_BITOR_in_logicExpr355 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_multExpr_in_logicExpr358 = new BitSet(new long[]{0x00000000000681F2L});
	public static final BitSet FOLLOW_BITNAND_in_logicExpr368 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_multExpr_in_logicExpr371 = new BitSet(new long[]{0x00000000000681F2L});
	public static final BitSet FOLLOW_BITNOR_in_logicExpr381 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_multExpr_in_logicExpr384 = new BitSet(new long[]{0x00000000000681F2L});
	public static final BitSet FOLLOW_atom_in_multExpr410 = new BitSet(new long[]{0x0000000000404202L});
	public static final BitSet FOLLOW_MUL_in_multExpr420 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_atom_in_multExpr423 = new BitSet(new long[]{0x0000000000404202L});
	public static final BitSet FOLLOW_DIV_in_multExpr433 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_atom_in_multExpr436 = new BitSet(new long[]{0x0000000000404202L});
	public static final BitSet FOLLOW_POW_in_multExpr446 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_atom_in_multExpr449 = new BitSet(new long[]{0x0000000000404202L});
	public static final BitSet FOLLOW_INT_in_atom475 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_atom483 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LP_in_atom491 = new BitSet(new long[]{0x0000000000001C00L});
	public static final BitSet FOLLOW_expr_in_atom494 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_RP_in_atom496 = new BitSet(new long[]{0x0000000000000002L});
}
